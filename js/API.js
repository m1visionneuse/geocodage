/*
Ce fichier comprend les trois fonctions des requêtes des trois apis de
géocodage ainsi que la fonction de changement de carte et celle de calcul du récapitulatif.
*/

//LES VARIABLES GLOBALES
var markerIGN;
var MarkerBAN;
var MarkerGoogle;
var saisie_user = document.getElementById("text") ;
var situation_ban = document.getElementById('lieu_ban') ;
var situation_ign = document.getElementById('lieu_ign') ;
var situation_google = document.getElementById('lieu_google') ;
var verif=[0,0,0];

//LES EVENEMENTS
document.getElementById('button').addEventListener('click',get);
document.getElementById("selection").addEventListener('click',afficher) ;
document.getElementById("fond_ortho").addEventListener("click", afficher_ortho_ign);
document.getElementById("carte_ign").addEventListener("click", afficher_carte_ign);

//LES FONCTIONS
function precisionRound(number, precision) {
  /*
  Cette fonction arrondi un nombre au nombre de décimale voulu.
  Param number : il s’agit du nombre que l’on souhaite arrondir.
  Param precision : il s’agit du nombre de décimale voulu ; ce nombre doit être un entier.
  Return : le nombre arrondi.
  */
  var factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
}

function get(){
  /*
  Cette fonction permet de lancer les différentes requêtes.
  */
  verif=[0,0,0];
  get_Ban();
  get_google();
  get_geoportail();
}

function get_geoportail (){
  /*
  Envoie une requête ajax vers l’api de l'ign, les data envoyées comprennent la clef
  si elle existe et la saisie de l’utilisateur eu moment de déclenchement, le
  callback des requêtes modifie le tableau de résultat de l'ign, insert le tableau
  html de résultat dans la section résultat de l'ign et crée ou modifie la
  localisation du marqueur de l'ign.
  */
    var features ;
    // addresse saisie par l'utilisateur
    var addresse = saisie_user.value;
    // encoder l'addresse afin de la passer dans l'url
    var encoded_addresse = encodeURI(addresse) ;
    /* requète ajax */
    var ajax = new XMLHttpRequest() ;
    ajax.open('GET', 'http://wxs.ign.fr/ysxcztwpwi01twxexn5lomnw/geoportail/ols?xls=%3c%3fxml+version%3d%221.0%22+encoding%3d%22UTF-8%22%3f%3e%0d%0a%3cXLS%0d%0a++xmlns%3agml%3d%22http%3a%2f%2fwww.opengis.net%2fgml%22%0d%0a++xmlns%3d%22http%3a%2f%2fwww.opengis.net%2fxls%22%0d%0a++xmlns%3axsi%3d%22http%3a%2f%2fwww.w3.org%2f2001%2fXMLSchema-instance%22+version%3d%221.2%22%0d%0a++xsi%3aschemaLocation%3d%22http%3a%2f%2fwww.opengis.net%2fxls+http%3a%2f%2fschemas.opengis.net%2fols%2f1.2%2folsAll.xsd%22%3e%0d%0a++%3cRequestHeader+srsName%3d%22epsg%3a4326%22%2f%3e%0d%0a++%3cRequest+maximumResponses%3d%2225%22+methodName%3d%22GeocodeRequest%22+requestID%3d%22uid42%22+version%3d%221.2%22%3e%0d%0a++%3cGeocodeRequest+returnFreeForm%3d%22false%22%3e%0d%0a++++%3cAddress+countryCode%3d%22StreetAddress%22%3e%0d%0a++++++%3cfreeFormAddress%3e'+encoded_addresse+'%3c%2ffreeFormAddress%3e%0d%0a++++%3c%2fAddress%3e%0d%0a++%3c%2fGeocodeRequest%3e%0d%0a++%3c%2fRequest%3e%0d%0a%3c%2fXLS%3e', true) ;
    ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    ajax.addEventListener('readystatechange',  function(e) {
      if(ajax.readyState == 4 && ajax.status == 200) {
          features = ajax.responseText ;
          // ouverture d'un nouveau parser xml
          parser = new DOMParser();
          // transformer en chaine de caractère le fichier xml
          xmlDoc = parser.parseFromString(features,"text/xml");
          // insertion des coordonnées xml dans la page html
          var resultat = document.getElementById("ign");
          // var score = xmlDoc.getElementsByTagName("GeocodedAddress")[0].childNodes[0] ;
          var score = xmlDoc.getElementsByTagName("GeocodeMatchCode")[0].attributes[0].nodeValue ;
          // récupération du nombre de résultats renvoyé par la requête
          var nb_res = xmlDoc.getElementsByTagName("GeocodeResponseList")[0].childNodes[0].parentElement.childElementCount;
          ign.innerHTML="";
          // parcours des trois premiers resultats si ils existent
          for (var i=0; i<3 & i<nb_res; i++){
            // pour chaqu'un on recupere les parametres importants
            var coord=xmlDoc.getElementsByTagName("gml:pos")[i].childNodes[0].nodeValue;
            var accuracy = Number(xmlDoc.getElementsByTagName("GeocodeMatchCode")[i].attributes[0].nodeValue) ;
            // extraction des coordonnées et du score
            var latt = coord.substr(0, 9) ;
            var long = coord.substr(10, 19) ;
            var score = precisionRound(accuracy, 6) ;
            ign.innerHTML+="<div class=test><div class='table-responsive'><table class='table table-stripped'><thead class='thead-inverse'><tr><th>&nbsp;</th><th>résultat " +(i+1)+"</th></tr></thead>"+
            "<tbody><tr><th>Long</th><td>"+long+"</td></tr>"+
            "<tr><th>Lat</th><td>"+latt+"</td></tr>"+
            "<tr><th>Score</th><td>"+score+"</td></tr></tbody></table></div></div>" ;
          }
          // affichage de l'addresse complète du premier résultat retourné par le service
          situation_ign.innerHTML = "" ;
          if (xmlDoc.getElementsByTagName("Street")[0].innerHTML == ""){
            situation_ign.innerHTML ="Lieu: "+ xmlDoc.getElementsByTagName("Place")[2].childNodes[0].nodeValue+" "+ xmlDoc.getElementsByTagName("Place")[0].childNodes[0].nodeValue ;
          }else if(xmlDoc.getElementsByTagName("Place")[2].innerHTML == ""){
            situation_ign.innerHTML = "Lieu: "+ xmlDoc.getElementsByTagName("Place")[0].childNodes[0].nodeValue ;
          }
          else {
            situation_ign.innerHTML = "Lieu: "+ xmlDoc.getElementsByTagName("Street")[0].innerHTML+" "+xmlDoc.getElementsByTagName("PostalCode")[0].childNodes[0].nodeValue+" "+ xmlDoc.getElementsByTagName("Place")[0].childNodes[0].nodeValue ;
          }               

          //affichage grahqie du resultat
           lat_ign = Number(xmlDoc.getElementsByTagName("gml:pos")[0].childNodes[0].nodeValue.substr(0, 9));
           lng_ign = Number(xmlDoc.getElementsByTagName("gml:pos")[0].childNodes[0].nodeValue.substr(10, 19));
           var logo = L.icon({
             iconUrl : 'img/marker_ign.png',
             iconSize : [32, 32],
            });
            if(markerIGN == null){
              //création du marqueur
              markerIGN = L.marker([lat_ign,lng_ign],{
                icon:logo,
                draggable:false,
                opacity:0.8
              });
              markerIGN.addTo(map);
            }else {
              // mise à jour du marker IGN lorsqu'on change d'addresse
              markerIGN.setLatLng([lat_ign, lng_ign]).update();
            }
            //confirmation d'execution
            verif[0]=1;
            //verification de fin de toutes les requetes asynchrones
            if (verif[0]==1&verif[1]==1&verif[2]==1){
              recap();
              verif=[0,0,0];
            }
      }
  });
  ajax.send();
}

function get_Ban(){
  /*
  Envoie une requête ajax vers l’api de la ban, les data envoyées comprennent
  la clef si elle existe et la saisie de l’utilisateur eu moment de déclenchement,
  le callback des requêtes modifie le tableau de résultat de la ban, insert le tableau
  html de résultat dans la section résultat de la ban et crée ou modifie la
  localisation du marqueur de la ban.
  */
  var ban=document.getElementById('ban');
  ban.innerHTML="";
  // Récupération de l'adresse saisie
  var address=saisie_user.value;
  // Création de l'objet xhr
  var ajax = new XMLHttpRequest();
  // Destination et type de la requête AJAX (asynchrone ou non)
  ajax.open('get','https://api-adresse.data.gouv.fr/search/?q='+address, true);
  // métadonnées de la requête AJAX
  ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  // Evènement de changement d'état de la requête
  ajax.addEventListener('readystatechange',  function(e) {
  // Vérifier que la requête a été envoyé et que le serveur a répondu et que la ressource est trouvée
    if(ajax.readyState == 4 && ajax.status == 200) {
      // Enregistrement du résultat dans un tableau
      var result = JSON.parse(ajax.responseText);
      // Parcours des trois premiers resultats si ils existent
      for (var i = 0; i < 3 & i<result.features.length; i++) {
        // Récupératriondes variables Longitude,Latitude et score
        var Longitude = precisionRound(result.features[i].geometry.coordinates[0], 6) ;
        var Latitude = precisionRound(result.features[i].geometry.coordinates[1], 6) ;
        var score = precisionRound(result.features[i].properties.score, 6);
        ban.innerHTML+="<div class=test><div class='table-responsive'><table class='table table-stripped'><thead class='thead-inverse'><tr><th>&nbsp;</th><th>résultat"+(i+1)+"</th></tr></thead>"+
        "<tbody><tr><th>Long</th><td>"+Longitude+"</td></tr>"+
        "<tr><th>Lat</th><td>"+Latitude+"</td></tr>"+
        "<tr><th>Score</th><td>"+score+"</td></tr></tbody></table></div></div>";
      }
      
      // affichage de l'addresse du premier résultat retourné par le service
      situation_ban.innerHTML = "" ;
      situation_ban.innerHTML = "Lieu : " + result.features[0].properties.label ;
       // Representation graphique du resultat
       lat_ban = result.features[0].geometry.coordinates[1];
       lng_ban = result.features[0].geometry.coordinates[0];
       var logo = L.icon({
         iconUrl : 'img/marker_ban.png',
         iconSize : [32, 32],
       });
       if(MarkerBAN == null){
         // Ajout du marker pour la ban au fond de carte IGN
         MarkerBAN = L.marker([lat_ban,lng_ban],{
           icon:logo,
           draggable:false,
           opacity:0.8
         })
         MarkerBAN.addTo(map);
       }else {
         //Deplacement du marqueur existant
         MarkerBAN.setLatLng([lat_ban, lng_ban]).update()
       }
       //confirmation d'execution
       verif[1]=1;
       //verification de fin de toutes les requetes asynchrones
       if (verif[0]==1&verif[1]==1&verif[2]==1){
         recap();
         verif=[0,0,0];
       }
     }
  });
  // Envoi de la requête
  ajax.send();
}

function get_google(){
  /*
  Envoie une requête ajax vers l’api google, les data envoyées comprennent la clef
  si elle existe et la saisie de l’utilisateur eu moment de déclenchement,
  le callback des requêtes modifie le tableau de résultat de google, insert le
  tableau html de résultat dans la section résultat de google et crée ou modifie
  la localisation du marqueur de google.
  */
  var google=document.getElementById('google');
  var key='key=AIzaSyCw-OeLs7TS_A3YocEZXssDWuINOrRzyo8';
  // a la validation du champ text on recupere son contenu
  var address='address='+saisie_user.value;
  //clearMarkers();
  // on reinitialise le contenu de la div correspondant au resultats de google
  google.innerHTML="";
  //on requete l'api avec l'addresse recuperée dans le champs text et la clef
  var ajaxG = new XMLHttpRequest();
  ajaxG.open('POST', 'https://maps.googleapis.com/maps/api/geocode/json?'+address+'&'+key+'&components=country:FR', true);
  ajaxG.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  ajaxG.addEventListener('readystatechange',  function (e)  {
    if(ajaxG.readyState == 4 && ajaxG.status == 200) {
      // on separe le resultat en Json
      var resGG=JSON.parse(ajaxG.responseText);
      //Parcours des trois premiers resultats si ils existent
      for (var i=0; i<3 & i<resGG.results.length; i++){
        // pour chaqu'un on recupere les parametres importants
        var latitude_google=precisionRound(resGG.results[i].geometry.location.lat, 6);
        var longitude_google=precisionRound(resGG.results[i].geometry.location.lng, 6);
        var score='inconnu';
        // on les integres dans la page HTML.
        google.innerHTML+="<div class=test><div class='table-responsive'><table class='table table-stripped'><thead class='thead-inverse'><tr><th>&nbsp;</th><th>résultat " +(i+1)+"</th></tr></thead>"+
        "<tbody><tr><th>Long</th><td>"+longitude_google+"</td></tr>"+
        "<tr><th>Lat</th><td>"+latitude_google+"</td></tr>"+
        "<tr><th>Score</th><td>"+score+"</td></tr></tbody></table></div></div>";
      }
      // affichage de l'addresse du premier résultat retourné par le service
      situation_google.innerHTML = "" ;
      situation_google.innerHTML ="Lieu : " + resGG.results[0].formatted_address ;

      // création du marker google pour l'insertion dans la page avec le fond ortho ign
      var logo = L.icon({
        iconUrl : 'img/marker_google.png',
        iconSize : [32, 32],
      });
      lat_google = resGG.results[0].geometry.location.lat;
      lng_google = resGG.results[0].geometry.location.lng;

      if(MarkerGoogle == null){
        MarkerGoogle = L.marker([lat_google,lng_google],{
          icon:logo,
          draggable:false,
          opacity:0.8
        })
        MarkerGoogle.addTo(map);
      }else {
        //Deplaceemnt du marqueur existant
        MarkerGoogle.setLatLng([lat_google, lng_google]).update()
      }
      map.setView([latitude_google, longitude_google], 8);
      //Confirmation d'execution
      verif[2]=1;
      //verification de fin des trois requetes asynchrones.
      if (verif[0]==1&verif[1]==1&verif[2]==1){
        recap();
        verif=[0,0,0];
      }
    }
  });
  ajaxG.send();
}
var recapit=document.getElementById("rec");
function recap(){
  /*
  Cette fonction utilise les données des tableaux de résultats des trois requêtes
  pour calculer et afficher dans la page un tableau récapitulatif. Ce tableau
  donne pour chaque api les écarts sur x (la latitude) et y (la longitude) entre
  le point obtenu et le centre de gravité des points des 3 api pour le résultat 1.
  */
    var testgg= document.querySelector(".corp #resultats #google .test").innerText;
    var testban= document.querySelector(".corp #resultats #ban .test").innerText;
    var testign= document.querySelector(".corp #resultats #ign .test").innerText;

    var xc=(lat_ign+lat_ban+lat_google)/3;
    var yc=(lng_ign+lng_ban+lng_google)/3;
    recapit.innerHTML="<div class='table-responsive'><table class='table table-stripped'><thead class='thead-inverse'><tr><th>résultat 1</th><th>Difference x (°)</th><th>Difference y (°)</th></tr></thead>"+
    "<tbody><tr><th>IGN</th><td>"+precisionRound((xc-lat_ign), 4)+"</td><td>"+precisionRound((yc-lng_ign),4)+"</td></tr>"+
    "<tr><th>BAN</th><td>"+precisionRound((xc-lat_ban), 4)+"</td><td>"+precisionRound((yc-lng_ban),4)+"</td>"+
    "<tr><th>Google</th><td>"+precisionRound((xc-lat_google), 4)+"</td><td>"+precisionRound((yc-lng_google),4)+"</td></tr></tbody></table></div>";
    // centrage de la carte sur les résultats de la recherche
    map.setView([xc, yc], 7);
}


function afficher(){
  /*
  Cette fonction permet de détecter l’option sélectionné dans le menu choix de carte
  et d’afficher la carte correspondante.
  */
  //detection de la carte voulue
  if(document.getElementById('selection').value == 1){
    afficher_carte_ign();
  }else{
    afficher_ortho_ign();
  }
}

function afficher_ortho_ign(event){
  /*
  Cette fonction permet d’afficher la vue satellite issue de la bd ortho.
  Le changement de carte réinitialise les résultats.
  */
  document.getElementById('carte').innerHTML = "";
  ign.innerHTML="";
  google.innerHTML="";
  ban.innerHTML="";
  recapit.innerHTML="";
  situation_ign.innerHTML = "" ;
  situation_ban.innerHTML = "" ;
  situation_google.innerHTML = "" ;
  markerIGN=null;
  MarkerBAN=null;
  MarkerGoogle=null;
  map.remove();
  // ajout de la carte
  window.map  = L.map('carte', {
    zoom : 8,
    center : L.latLng(47.3685,3.279589)
   });
   // Création de la couche
   var lyr = L.geoportalLayer.WMTS({
       layer  : "ORTHOIMAGERY.ORTHOPHOTOS"
   });
   lyr.addTo(map);
}

function afficher_carte_ign(){
  /*
  Cette fonction permet d’afficher le fond de carte IGN standard.
  Le changement de carte réinitialise les résultats.
  */
    document.getElementById('carte').innerHTML = "";
    ign.innerHTML="";
    google.innerHTML="";
    ban.innerHTML="";
    recapit.innerHTML="";
    situation_ign.innerHTML = "" ;
    situation_ban.innerHTML = "" ;
    situation_google.innerHTML = "" ;
    markerIGN=null;
    MarkerBAN=null;
    MarkerGoogle=null;
    map.remove();
    // ajout de la carte
    window.map  = L.map('carte', {
       zoom : 8,
       center : L.latLng(47.3685,3.279589)
    });
    // Création de la couche
    var lyr = L.geoportalLayer.WMTS({
         layer  : "GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD"
     });
     lyr.addTo(map);
}
